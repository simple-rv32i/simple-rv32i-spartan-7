--! @file verificationutils.vhd
--! @brief Utility package for verification
--!
--! @details General purpose utilities for vhdl testbenches.
--!
--! @author Markus Wagner
--!
--! @copyright Markus Wagner, 2022
--!
--! Permission is hereby granted, free of charge, to any person obtaining a copy
--! of this software and associated documentation files (the "Software"), to
--! deal in the Software without restriction, including without limitation the
--! rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
--! sell copies of the Software, and to permit persons to whom the Software is
--! furnished to do so, subject to the following conditions:
--!
--! The above copyright notice and this permission notice shall be included in
--! all copies or substantial portions of the Software.
--!
--! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
--! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
--! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
--! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
--! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
--! FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
--! IN THE SOFTWARE.
--!
library ieee;
use ieee.std_logic_1164.all;

package verificationutils is
    --! Default clock period value
    constant CLK_PERIOD: time := 5 ns;

    --! @param clk_in       copy of clock signal
    --! @param n            number of falling edges, which should be waited on
    --!
    --! wait for n falling edges of clk_in
    --!
    --! **usage**
    --! \`\`\`vhdl
    --! wait_clk_cycles(clk, 42);
    --! \`\`\`
    procedure wait_clk_cycles(signal clk_in: in std_logic; n: in natural);
end package verificationutils;


package body verificationutils is
    procedure wait_clk_cycles(signal clk_in: in std_logic; n: in natural) is
    begin
        for idx in 0 to n - 1 loop
            wait until falling_edge(clk_in);
        end loop;
    end procedure wait_clk_cycles;
end package body verificationutils;