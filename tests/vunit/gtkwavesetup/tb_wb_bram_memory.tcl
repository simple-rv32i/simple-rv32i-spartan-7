puts "cleaning view"
set displaylist [ gtkwave::getDisplayedSignals ]
puts "$displaylist"
set num_deleted [ gtkwave::deleteSignalsFromListIncludingDuplicates $displaylist ]
puts "setting up view"
set label_stimuli [list]
lappend label_stimuli "stimuli"
set num_added [ gtkwave::addCommentTracesFromList $label_stimuli ]
set stimuli_traces [list]
lappend stimuli_traces "top.tb_wb_bram_memory.rst"
lappend stimuli_traces "top.tb_wb_bram_memory.clk"
lappend stimuli_traces "top.tb_wb_bram_memory.cyc"
lappend stimuli_traces "top.tb_wb_bram_memory.stb"
lappend stimuli_traces "top.tb_wb_bram_memory.we"
lappend stimuli_traces "top.tb_wb_bram_memory.sel"
lappend stimuli_traces "top.tb_wb_bram_memory.adr"
lappend stimuli_traces "top.tb_wb_bram_memory.data_i"
set num_added [ gtkwave::addSignalsFromList $stimuli_traces ]

set label_dut [list]
lappend label_dut "UUT"
set num_added [ gtkwave::addCommentTracesFromList $label_dut ]

set uut_traces [list]
lappend uut_traces "top.tb_wb_bram_memory.uut.ctrl_state"
lappend uut_traces "top.tb_wb_bram_memory.uut.next_state"
lappend uut_traces "top.tb_wb_bram_memory.ack"
lappend uut_traces "top.tb_wb_bram_memory.data_o"
lappend uut_traces "top.tb_wb_bram_memory.gpo"
set num_added [ gtkwave::addSignalsFromList $uut_traces ]

puts "unzooming time"
set max_time [ gtkwave::getMaxTime ]
puts "$max_time"
gtkwave::setZoomRangeTimes 0 $max_time