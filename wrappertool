#!/usr/bin/env python3
# used to run standalone functionallity of this fpga wrapper plugin
# to use the wrapper from a cpu project, see wrapperlib.py
import click
import logging
import subprocess
import toml
from vunit import VUnit


def generatememory(outfilename: str, infilename: str):
    """filename without extention"""
    with open("resources/templates/start-wb-bram-memory.vhdl.template") as f:
        frontdata = f.read()
    with open("resources/templates/end-wb-bram-memory.vhdl.template") as f:
        enddata = f.read()
    maxmemadr = 0x80
    memdata = []
    if infilename == "":
        memdata = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    else:
        with open(infilename, "rb") as f:
            indata = f.read(-1)
            for datum in indata:
                memdata = memdata + [int(datum)]
    with open(outfilename, "w") as fout:
        fout.write(frontdata + "\n")
        for i in range(0, maxmemadr):
            fout.write('        INIT_{0:02X} => X"'.format(i))
            for idx in range(0, 32):
                dataidx = (i + 1) * 32 - 1 - idx
                if dataidx < len(memdata):
                    fout.write("{:02X}".format(memdata[dataidx]))
                else:
                    fout.write("FF")
            if i < maxmemadr - 1:
                fout.write('",\n')
            else:
                fout.write('")\n')
        fout.write(enddata)


@click.group(
    help=(
        "CLI tool to interface with documentation, testing,"
        "simulation and other standalone functionallity"
    )
)
@click.option(
    "--loglevel",
    type=click.Choice(["debug", "info", "warn", "error"], case_sensitive=False),
    default="warn",
)
def cli(loglevel):
    levelenum = logging.WARN
    if loglevel == "debug":
        levelenum = logging.DEBUG
    elif loglevel == "info":
        levelenum = logging.INFO
    elif loglevel == "warn":
        levelenum = logging.WARN
    elif loglevel == "error":
        levelenum = logging.ERROR
    logging.basicConfig(level=levelenum)
    pass


@cli.command(
    context_settings=dict(
        ignore_unknown_options=True,
    )
)
@click.option("-h", "--help", is_flag=True)
@click.option("--version", is_flag=True)
@click.option("--pathcfg", default="local/paths.toml", show_default=True)
@click.argument("vunitparams", nargs=-1, type=click.UNPROCESSED)
def test(help, version, pathcfg, vunitparams):
    """Wrapper around vunit, see wrappertool test --help for more information"""
    params = vunitparams
    if version is True:
        params = ("--version",) + params
    if help is True:
        params = ("--help",) + params
        click.echo("This is a wrapper around vunits standard interface.")
        click.echo("\tsee the following help page for <vunit params> but use")
        click.echo('\t"wrappertool test <vunit params>" instead of')
        click.echo('\t"wrappertool <vunit params>"')
        click.echo("========== 'vunit --help' output ==========\n")
    if help is False:
        # prepare simulation
        generatememory("src/wb_bram_check_memory.vhd", "")
    pathcfg = toml.load(pathcfg)
    vu = VUnit.from_argv(argv=params)
    lib = vu.add_library("tblib")
    lib.add_source_files("tests/vunit/tb/*.vhd")
    lib.add_source_files("tests/verification/*.vhd")
    lib.add_source_file("src/wb_bram_check_memory.vhd")
    vu.add_external_library("unisim", pathcfg["sim"]["unisim"])
    vu.add_external_library("unimacro", pathcfg["sim"]["unimacro"])
    vu.add_compile_option("ghdl.a_flags", ["-fsynopsys"])
    vu.set_sim_option("ghdl.elab_flags", ["-frelaxed", "-fsynopsys"])
    # add configs/generics
    tb_wb_bram_memory = lib.test_bench("tb_wb_bram_memory")
    for testcase in tb_wb_bram_memory.get_tests():
        if testcase.name == "write_multiple_bytes":
            for offset in range(0, 4):
                testcase.add_config(
                    name="offset_{}".format(offset),
                    generics=dict(check_byte_offset=offset),
                )
    # register tcl waveform setup
    try:
        tb_controller = lib.test_bench("tb_wb_bram_memory")
        tb_controller.set_sim_option(
            "ghdl.gtkwave_script.gui", "tests/vunit/gtkwavesetup/tb_wb_bram_memory.tcl"
        )
    finally:
        pass
    vu.main()


@cli.command()
@click.option(
    "--pathcfg",
    default="local/paths.toml",
    type=click.Path(exists=True),
    show_default=True,
)
@click.option(
    "--cpupath",
    required=True,
    type=click.Path(exists=True),
    help="path to simple RV32I repository",
)
def genproj(pathcfg, cpupath):
    """generates the Vivado project"""
    # generate dummy memory
    subprocess.check_call(["make"], cwd="resources/templates/firmware/")
    generatememory(
        "src/wb_bram_memory.vhd", "resources/templates/firmware/firmware.bin"
    )
    # vivado
    pcfg = toml.load(pathcfg)
    vivadopath = pcfg["tools"]["vivado"]
    logging.debug("vivado path: '{}'".format(vivadopath))
    subprocess.check_call(
        [
            vivadopath,
            "-nolog",
            "-mode",
            "batch",
            "-source",
            "generateprj.tcl",
            "-tclargs",
            cpupath,
        ]
    )


@cli.command()
@click.option(
    "-b",
    "--bin",
    help="bin file, from which the memory is generated",
    type=click.Path(exists=True),
    required=True,
)
def genmem(bin):
    """generate the memory file from a .bin"""
    generatememory("src/wb_bram_memory.vhd", bin)


@cli.command()
@click.option("--pathcfg", default="local/paths.toml", show_default=True)
def launchvivado(pathcfg):
    """start the generated project in Xilinx Vivado(TM)"""
    pcfg = toml.load(pathcfg)
    vivadopath = pcfg["tools"]["vivado"]
    logging.debug("vivado path: '{}'".format(vivadopath))
    subprocess.call(
        [
            vivadopath,
            "-nolog",
            "simplerv32i_spartan7/simplerv32i_spartan_7.xpr",
        ]
    )


if __name__ == "__main__":
    cli()
