----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/07/2022 10:16:07 AM
-- Design Name: 
-- Module Name: clock_devider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_devider is
    Generic (
           CntWidth : natural
    );
    Port (
           clk : in STD_LOGIC;
           clk_out : out STD_LOGIC);
end clock_devider;

architecture Behavioral of clock_devider is
    signal counter: unsigned(CntWidth - 1 downto 0) := to_unsigned(0, CntWidth);
begin
    clk_out <= counter(CntWidth - 1);
    upcounter: process(clk) is
    begin
        if rising_edge(clk) then
            counter <= counter + to_unsigned(1, CntWidth);
        end if;
    end process;
end Behavioral;
