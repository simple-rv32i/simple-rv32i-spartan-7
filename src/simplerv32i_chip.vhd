library ieee;
use ieee.std_logic_1164.all;


entity simplerv32i_chip is
    port(
        clk:        in  std_logic;
        btn:        in  std_logic_vector(1 downto 0);
        led:        out std_logic_vector(3 downto 0);
        pio2:       out std_logic;
        pio3:       out std_logic;
        pio4:       out std_logic;
        pio5:       out std_logic;
        pio6:       out std_logic;
        pio7:       out std_logic;
        pio8:       out std_logic;
        pio9:       out std_logic;
        pio16:      out std_logic;
        pio17:      out std_logic;
        pio18:      out std_logic;
        pio19:      out std_logic;
        pio20:      out std_logic;
        pio21:      out std_logic;
        pio22:      out std_logic;
        pio23:      out std_logic;
        pio26:      out std_logic
    );
end entity simplerv32i_chip;

architecture structural of simplerv32i_chip is
    type t_rststate is (s_rst0, s_rst1, s_idle);
    signal rst_state, rst_next_state: t_rststate := s_rst0;
    signal slow_clk: std_logic;
    signal wb_adr, wb_dat_mosi, wb_dat_miso: std_logic_vector(31 downto 0);
    signal wb_sel: std_logic_vector(3 downto 0);                 
    signal wb_stb, wb_cyc, wb_we, wb_ack: std_logic;
    signal dbg_ld_ir, dbg_pc_not_alu, dbg_ld_pc, dbg_adr_pc_not_alu: std_logic;
    signal gpo: std_logic_vector (3 downto 0);
    
    signal rst : std_logic := '1';
begin
    led <= gpo;
    
    pio2 <= wb_adr(0);
    pio3 <= wb_adr(1);
    pio4 <= wb_adr(2);
    pio5 <= wb_adr(3);
    pio6 <= wb_dat_miso(0);
    pio7 <= wb_dat_miso(1);
    pio8 <= wb_dat_miso(2);
    pio9 <= wb_dat_miso(3);
    
    pio16 <= slow_clk;
    pio17 <= wb_we;
    pio18 <= wb_cyc;
    pio19 <= wb_ack;
    pio20 <= dbg_ld_ir;
    pio21 <= dbg_pc_not_alu;
    pio22 <= dbg_ld_pc;
    pio23 <= dbg_adr_pc_not_alu;

    pio26 <= rst;
    
    --clkdiv: entity work.clock_devider generic map(
    --    CntWidth => 23
    --)
    --port map(
    --    clk => clk,
    --    clk_out => slow_clk
    --);
    slow_clk <= clk;

    memory: entity work.wb_bram_memory port map(
        rst => rst,
        clk => slow_clk,
        adr => wb_adr,
        dat_i => wb_dat_mosi,
        dat_o => wb_dat_miso,
        we => wb_we,
        sel => wb_sel,
        stb => wb_stb,
        ack => wb_ack,
        cyc => wb_cyc,
        gpo => gpo
    );
    
    cpu: entity work.riscy_cpu port map(
        clk => slow_clk,
        reset => rst,
        adr => wb_adr,
        data_i => wb_dat_miso,
        data_o => wb_dat_mosi,
        we => wb_we,
        sel => wb_sel,
        stb => wb_stb,
        cyc => wb_cyc,
        ack => wb_ack,
        dbg_ld_ir => dbg_ld_ir,
        dbg_pc_not_alu => dbg_pc_not_alu,
        dbg_ld_pc => dbg_ld_pc,
        dbg_adr_pc_not_alu => dbg_adr_pc_not_alu
    );
    
    rstgen_statetransfer: process(slow_clk) is
    begin
        if rising_edge(slow_clk) then
            rst_state <= rst_next_state;
        end if;
    end process;
    
    rstgen_statecoder: process(rst_state, rst_next_state, btn(0)) is
    begin
        rst_next_state <= rst_state;
        rst <= '1';
        case rst_state is
            when s_rst0 =>
                rst <= '1';
                if btn(0) = '0' then
                    rst_next_state <= s_rst1;
                end if;
            when s_rst1 =>
                if btn(0) = '0' then
                    rst_next_state <= s_idle;
                end if;
            when s_idle =>
                rst <= '0';
                if btn(0) = '1' then
                    rst_next_state <= s_rst0;
                end if;
        end case;
    end process;
end architecture structural;
