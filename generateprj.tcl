# === Configure parameters ===
set proj_dir "simplerv32i_spartan7/"
set proj_name "simplerv32i_spartan_7"
if { $argc != 1 } {
    puts "generator needs 1 argument, but $argc are given"
    exit 1
} else {
    set rv32i_dir [lindex $argv 0]
    puts "cpu path: $rv32i_dir"
}

set rv32i_files [list \
    "add4.vhd" \
    "alu.vhd" \
    "controller.vhd" \
    "cpu.vhd" \
    "instructionset.vhd" \
    "ir.vhd" \
    "pc.vhd" \
    "regfile.vhd" \
    "regsortmux.vhd" \
    "simplerv32i_package.vhd" \
]
set peripheral_files [list \
    "clock_devider.vhd" \
    "wb_bram_memory.vhd" \
]
set boardconstraintfile "cmod-s7-25-viscy.xdc"
set toplevelfile "simplerv32i_chip.vhd"
set toplevelname "SIMPLERV32I_CHIP"

# === Create project ===
file delete -force $proj_dir
file mkdir -force $proj_dir
create_project $proj_name ./$proj_dir -part xc7s25csga225-1
# Set project properties
set obj [current_project]
set_property -name "board_part_repo_paths" -value "[file normalize resources/board/]" -objects $obj
set_property -name "board_part" -value "digilentinc.com:cmod-s7-25:part0:1.0" -objects $obj
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "enable_vhdl_2008" -value "1" -objects $obj
set_property -name "mem.enable_memory_map_generation" -value "1" -objects $obj
set_property -name "revised_directory_structure" -value "1" -objects $obj
set_property -name "simulator_language" -value "VHDL" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj
# add files
# source files viscy
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}
foreach ifile $rv32i_files {
    set currentfile [file normalize [file join $rv32i_dir "src/hw/" $ifile]]
    puts "add file $currentfile"
    add_files -fileset sources_1 $currentfile
    set_property file_type {VHDL 2008} [get_files $currentfile]
}
# source files periphral
foreach ifile $peripheral_files {
    set currentfile [file normalize "src/$ifile"]
    puts "add file $currentfile"
    add_files -fileset sources_1 $currentfile
    set_property file_type {VHDL 2008} [get_files $currentfile]
}
# add toplevel file
add_files -fileset sources_1 [file normalize "src/$toplevelfile"]
set obj [get_filesets sources_1]
# register toplevel entity
set_property -name "top" -value "$toplevelname" -objects $obj
# constraints
# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}
set constr_file [file normalize "resources/board/$boardconstraintfile"]
add_files -norecurse -fileset constrs_1 $constr_file
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$constr_file"]]
set_property -name "file_type" -value "XDC" -objects $file_obj
set_property file_type {VHDL 2008} [get_files *.vhd]