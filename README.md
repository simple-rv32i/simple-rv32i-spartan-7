# Cmod-S7 CPU hosting wrapper

This project was developed to run a [simple RISC-V 32I cpu implementation](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i) as a softcore on the Spartan 7 of the [Digilent Cmod S7](https://digilent.com/reference/programmable-logic/cmod-s7/start) FPGA development board. It therefore provides clock, reset, a synchronous wishbone client memory and 4 green LEDs as output to the cpu. Theoretically, it could be modivied to be used with other simple custom cpu projects as well.

## Project setup

The `wrappertool` provides a common entry point for different tasks. In order to use this tool and repository, the following software must be installed:

* Python 3
* Python libraries, as specified in [requirements.txt](./requirements.txt). They can be installed, i.e. by using the pip command: `pip install -r requirements.txt`
* ghdl (the debian package lacks some importan components and is not compatible with Xilinx unisim. Installation from source is strongly recommanded)
* gtkwave
* a precompiled Xilinx unisim/macro. See ghdl's script folder for more information
* Xilinx Vivado 2021.2 (other versions were not tested, but probably will also work)
* a main cpu project, i.e. [Simple RV32I](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i)
* optional: a [compatible simulator](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i-simulator) for software development
* `wrappertool` queries the paths to vivado and the compiled unisim /unimacro libary from `local/paths.toml`. Use [local/paths.toml.example](./local/paths.toml.example) as a self explainatory template.

## Using `wrappertool`

This tool provides a common entry point to all the different tools, that are involved. It provides subcommands for different stages of project usage:

```sh
# up to date help
./wrappertool --help
# or for subcommand usage
./wrappertool <subcommand name> --help
# run unit tests
./wrappertool test
# generate the vhdl memory file, containing the executable from a typical binary file, as can be generated from a `riscv64-unknown-elf` toolchain elf file with `riscv64-unknown-elf-objcopy`
./wrappertool genmem --bin <path to binary file>
# generate the fpga project for Xilinx Vivado IDE
./wrappertool genproj --cpupath <path to local copy simple-rv32i cpu core repository>
# open the project in Xilinx Vivado IDE GUI
./wrappertool launchvivado
```

Synthesis, implementation, bitstream generation and target flashing can be done inside Xilinx Vivado IDE.

## Project structure

| Folder / component | Description |
|-----------|-------------|
| `wrapppertool` | universal cli interface to different project paths |
| `resources/board/` | Cmod-S7 board files and constraints file |
| `resources/templates/` | templates for generating memory vhdl files |
| `resources/templates/firmware` | example firmware, makefile and linker script |
| `src/`         | contains vhdl sources |
| `src/wb_bram_check_memory.vhd` | memory generated for memory unit tests (to test the memory creation templates and its wishbone interface) |
| `src/wb_bram_memory.vhd` | gemerated memory for the target application |
| `tests/verification/` | vhdl verification helper functions |
| `tests/vunit/gtkwavesetup/` | tcl files to load some unit test trace signals into gtkwave |
| `tests/vunnit/tb/` | testbench for vhdl components |

### Components

The top level entity is [simplerv32i_chip](./src/simplerv32i_chip.vhd). It defines the interconnection between the individual components of the processor and the board pin assignment, i.e. LED output or some signals, to debug the cpu hardware. It also contains a synchronous reset signal generator. The processor itself consists of the following components:

### Clock devider

Optionally, a clock devider with a generic (static) devision factor can be used, i.e. to use the LEDs as debugging signals with a clock rate of approx. 1 Hz. The main clock signal operates at 12 MHz.

#### CPU

The CPU is located in its' own [repository](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i). It must provide a synchronous 32 bits wide wishbone master interface. The debug signals are not functionally required and can be deactivated in the top level entity of this project.

#### Memory and GP(I)O

This component utilizes a 36kbits blockram, which is part of all Xilinx 7 series fpgas, to provide ROM and RAM functionallity to the processor. At startup the ram is initialized with the data, provided during `wrappertool genmem` and must contain the executable code at the beginning. Therefore the blockram provides RAM and ROM functionallity simultaniously and has a synchronous wishbone slave interface, which contains a 32 bit data interface to the 8 bit addressable, little endian memory space. There are no protective measurements to prevent write access to the executable. The usable memory space extends from `0x0000` to `0x0999`, which are only 32 kbits. See [1] for more information an this decision. At last, this block provides also provides an interface to the 4 green status leds of the Cmod-S7. The values can be modified at the lower 4 bits at address `0x1000`. The complete word, starting at this address is readable and writeable and may support additional output in future.

### Testing

At the moment only tests for the wishbone memory are available. The tests can be run via `wrappertool test`. As the component tests of [the simple-rv32i project](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i), as both projects use the [VUnit testing framework](https://vunit.github.io). Opposed to the cpu project, this project needs additionally ghdl functionality, which currently is excluded from the debian packages and requires a installation from source. Also the testbench relies on Xlinix' unisim and unimacro library, which must be compiled in advance using the same simulator as vunit.

## Developing applications

In general, the application development depends on the cpu core project. This project only affects the linker script. A complete example of an assembler application for an unprivileged rv32i cpu is located in the [resources folder](./resources/templates/firmware/). The contained linkerscript can also be used for `C` firmware projects and the Makefile can easily be modified to support `C` applications.

## Copyright and Licensing

The project was developed by Markus Wagner and is licensed unter the MIT licnese. See [LICENSE](./LICENSE for further details). The board files (xml files in [resources/boards](./resources/board/)) were develeped by Digilent, Inc. licensed also under the MIT-license. The original Digilent repository can be found [here](https://github.com/Digilent/vivado-boards) The constraints file [resources/boards/cmod-s7-viscy.xdc](./resources/boards/cmod-s7-viscy.xdc) originates from another MIT-licensed [Digilent repository](https://github.com/Digilent/digilent-xdc), but was modified to meet the project requirements.

---

[1] The unused 4 kbits are excluded to minimize the complexity of this project. Xilinx Blockrams provide two input and two output ports. The data width of these data ports can be set to certain values. The address width depends on the data port width. As stated in [Xilinx UG953](https://docs.xilinx.com/v/u/2012.3-English/ug953-vivado-7series-libraries), data ports widths from 19 to 36 all have a address width of 9 bits. Therefore only 36 bit port width enables the access of all 36 kbits. But this would require additional address translation and data bit multiplexing logic. Based on [Xilinx UG473](https://docs.xilinx.com/v/u/en-US/ug473_7Series_Memory_Resources) One can guess that the reason for this unusual port width is, that the memory is intended to store a parity bit for each byte.
